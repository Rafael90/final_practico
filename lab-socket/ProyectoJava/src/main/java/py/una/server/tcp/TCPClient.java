package py.una.server.tcp;

import java.util.List;
import java.net.ServerSocket;
import java.net.Socket;
import py.una.entidad.ConvertirJSON;
import py.una.entidad.ConvertirObject;
import py.una.entidad.Clima;
import java.io.*;
import java.net.*;

public class TCPClient {

	public static void main(String[] args) throws IOException {

		Socket unSocket = null;
		PrintWriter out = null;
		BufferedReader in = null;
		String mensajeClient;
		String mensajeServer;
		
		try {
			unSocket = new Socket("localhost", 4444);
			// enviamos nosotros
			out = new PrintWriter(unSocket.getOutputStream(), true);

			// viene del servidor
			in = new BufferedReader(new InputStreamReader(unSocket.getInputStream()));
		} catch (UnknownHostException e) {
			System.err.println("Host desconocido");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Error de I/O en la conexion al host");
			System.exit(1);
		}

		BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
		String fromServer;
		String fromUser;
		String opcion;
		String consultaClima;
		Clima datoClima;
		datoClima= new Clima("","","","","","","");
		
		ventanaPrincipal();
		opcion = stdIn.readLine();
		//Mientras que sea la opcion 1 y 2 se va a ejecutar
		while ((opcion.trim().compareTo("2") == 0) || opcion.trim().compareTo("1") == 0) {
		
		if (opcion.trim().compareTo("1") == 0) {

			System.out.print("Id_estacion: ");
			String id_estacion = stdIn.readLine();
			
			System.out.print("Ciudad: ");
			String ciudad = stdIn.readLine();

			System.out.print("Promedio_humedad: ");
			String promedio_humedad = stdIn.readLine();
			
			System.out.print("Temperatura: ");
			String temperatura = stdIn.readLine();
			
			System.out.print("Velocidad_viento: ");
			String velocidad_viento = stdIn.readLine();
			
			System.out.print("Fecha: ");
			String fecha = stdIn.readLine();

			System.out.print("hora: ");
			String hora = stdIn.readLine();
			
			Clima cli = new Clima(id_estacion , ciudad, promedio_humedad,temperatura ,velocidad_viento,fecha, hora );

			fromUser = ConvertirJSON.generarClimaJSON("1", cli);

			System.out.println("Cliente: " + fromUser);
			out.println(fromUser);
			
			fromServer = in.readLine();
			 System.out.println("Servidor: " + fromServer);

		} else if (opcion.trim().compareTo("2") == 0) {
			
			try {
			System.out.println("Ingrese ciudad a consultar la temperatura");
			consultaClima = stdIn.readLine();
			mensajeClient = ConvertirJSON.smsJSON("2", consultaClima);
			System.out.println("Cliente: " + mensajeClient);
			out.println(mensajeClient);
			
			mensajeServer = in.readLine();
			System.out.println("Servidor: " + mensajeServer);
			
			int estado = ConvertirObject.obtenerEstado(mensajeServer);

			 } catch (IOException e) {
				 e.printStackTrace(); 
			 } catch (Exception e) {
			    e.printStackTrace(); 
			  }
			 
		
			}
		ventanaPrincipal();
		opcion = stdIn.readLine();
		}
		out.close();
		in.close();
		stdIn.close();
		unSocket.close();
		
	}

	public static void ventanaPrincipal() {
		System.out.println("╔══════════════════════════════════════╗");
		System.out.println("                   ║ Elija una opción                     ║");
		System.out.println("                   ║ 1. Agregar dato clima                ║");
		System.out.println("                   ║ 2. Consultar clima ciudad            ║");
		System.out.println("                   ║                                      ║");
		System.out.println("                   ╚══════════════════════════════════════╝");
		System.out.print("> ");
	}
	

	}






package py.una.server.tcp;


import java.net.*;
import java.io.*;
import java.util.List;

import java.util.ArrayList;
import java.net.ServerSocket;
import py.una.entidad.ConvertirJSON;
import py.una.entidad.ConvertirObject;
import py.una.entidad.Clima;

public class TCPServer2 {

    public static void main(String[] args) throws IOException {
    	
		List<Clima> ListaClima = new ArrayList<Clima>();
		
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(4444);
        } catch (IOException e) {
            System.err.println("No se puede abrir el puerto: 4444.");
            System.exit(1);
        }
        System.out.println("Puerto abierto: 4444.");
        Socket clientSocket = null;
        try {
            clientSocket = serverSocket.accept();
        } catch (IOException e) {
            System.err.println("Fallo el accept().");
            System.exit(1);
        }
        
        try {
        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(
                                                new InputStreamReader(
                                                clientSocket.getInputStream())
                                               );

        String inputLine, outputLine;
        int metodo;
        
        while ((inputLine = in.readLine()) != null) {
			System.out.println("Mensaje recibido: " + inputLine);

			if (inputLine.equals("Bye")) {
				out.println(inputLine);
				break;
			}

            metodo = ConvertirObject.obtenerMetodo(inputLine);
            String sms= ConvertirObject.obtenerMensaje(inputLine);

			// Metodo 1: Registro
			if (metodo == 1) {
				ListaClima.add(ConvertirObject.stringObjeto(inputLine));
				
				outputLine= ConvertirJSON.mensaJSON("Agregado");
				System.out.println("Alumno Rafael ricardo edad 32 años");
				out.println(outputLine);
				System.out.println("Servidor: " + outputLine);

				// Listar empresas
			} else if (metodo == 2) {
				outputLine = ConvertirJSON.mensajeDatoJSONUDP("0", sms , ListaClima);
				System.out.println("Servidor: " + outputLine);
				System.out.println("Alumno Rafael ricardo edad 32 años");
				out.println(outputLine);

			}
            
        }
        
        out.close();
        in.close();
        clientSocket.close();
        serverSocket.close();
        
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}

package py.una.entidad;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;

public class ConvertirObject {
	public static int obtenerMetodo(String str) throws Exception {
		JSONParser parser = new JSONParser();

		Object obj = parser.parse(str.trim());
		JSONObject jsonObject = (JSONObject) obj;

		int metodo = Integer.parseInt((String) jsonObject.get("metodo"));

		return metodo;
	}

	public static String obtenerMensaje(String str) throws Exception {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(str.trim());
		JSONObject jsonObject = (JSONObject) obj;
		String mensaje = (String) jsonObject.get("mensaje");

		return mensaje;
	}
	
	
	
	
	public static Clima stringObjeto(String str) throws Exception {
		Clima cli = new Clima("", "", "", "", "","","");
		JSONParser parser = new JSONParser();

		Object obj = parser.parse(str.trim());
		JSONObject jsonObject = (JSONObject) obj;

		obj = parser.parse(jsonObject.get("clima").toString().trim());
		jsonObject = (JSONObject) obj;

		cli.setId_estacion((String) jsonObject.get("id_estacion"));

		cli.setCiudad((String) jsonObject.get("ciudad"));
		cli.setPorcentaje_humedad((String) jsonObject.get("porcentaje_humedad"));
		cli.setTemperatura((String) jsonObject.get("temperatura"));
		cli.setVelocidad_viento((String) jsonObject.get("velocidad_viento"));
		cli.setFecha((String) jsonObject.get("fecha"));
		cli.setHora((String) jsonObject.get("hora"));
		
		return cli;
	}

	public static int obtenerEstado(String str) throws Exception {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(str.trim());
		JSONObject jsonObject = (JSONObject) obj;
		int estado = Integer.parseInt((String) jsonObject.get("estado"));

		return estado;
	}

}

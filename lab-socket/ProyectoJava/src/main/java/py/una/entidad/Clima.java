package py.una.entidad;

import java.util.ArrayList;
import java.util.List;

public class Clima {

	String id_estacion;
	String ciudad;
	String porcentaje_humedad;
	String temperatura;
	String velocidad_viento;
	String fecha;
	String hora;

	public Clima(String id_estacion, String ciudad, String porcentaje_humedad, String temperatura, String velocidad_viento, String fecha, String hora) {
		this.id_estacion = id_estacion;
		this.ciudad = ciudad;
		this.porcentaje_humedad = porcentaje_humedad;
		this.temperatura = temperatura;
		this.velocidad_viento = velocidad_viento;
		this.fecha = fecha;
		this.hora = hora;
	}

	public String getId_estacion() {
		return id_estacion;
	}

	public void setId_estacion(String id_estacion) {
		this.id_estacion = id_estacion;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getPorcentaje_humedad() {
		return porcentaje_humedad;
	}

	public void setPorcentaje_humedad(String porcentaje_humedad) {
		this.porcentaje_humedad = porcentaje_humedad;
	}


	public String getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(String temperatura) {
		this.temperatura = temperatura;
	}
	
	public String getVelocidad_viento() {
		return velocidad_viento;
	}

	public void setVelocidad_viento(String velocidad_viento) {
		this.velocidad_viento = velocidad_viento;
	}
	
	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}
}

package py.una.entidad;

import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ConvertirJSON {
	
	@SuppressWarnings("unchecked")
	public static String mensaJSON( String mensaje) {
		JSONObject obj = new JSONObject();
		obj.put("mensaje", mensaje);

		return obj.toJSONString();
	}
	

	@SuppressWarnings("unchecked")
	public static String smsJSON(String metodo, String mensaje) {
		JSONObject obj = new JSONObject();
		obj.put("metodo", metodo);
		obj.put("mensaje", mensaje);

		return obj.toJSONString();
	}
	
	
	@SuppressWarnings("unchecked")
	public static String mensajeDatoJSONUDP(String estado, String mensaje, List<Clima> lista) {
		JSONObject obj = new JSONObject();
		JSONArray listaMensaje = new JSONArray();

		obj.put("estado", estado);
		obj.put("mensaje", mensaje);

		for (Clima cli : lista) {
			if(cli.getCiudad().compareTo(mensaje) == 0) {

			JSONObject objCli = new JSONObject();
			objCli.put("temperatura", cli.getTemperatura());
			objCli.put("ciudad", cli.getCiudad());
			listaMensaje.add(objCli);
			}
		}

		obj.put("clima", listaMensaje);

		return obj.toJSONString();
	}

	@SuppressWarnings("unchecked")
	public static String generarClimaJSON(String metodo, Clima c) {
		JSONObject obj = new JSONObject();
		JSONObject objCli = new JSONObject();
		obj.put("metodo", metodo);

		objCli.put("id_estacion", c.getId_estacion());
		objCli.put("ciudad", c.getCiudad());
		objCli.put("porcentaje_humedad", c.getPorcentaje_humedad());
		objCli.put("temperatura", c.getTemperatura());
		objCli.put("velocidad_viento", c.getVelocidad_viento());
		objCli.put("fecha", c.getFecha());
		objCli.put("hora", c.getHora());

		obj.put("clima", objCli);

		return obj.toJSONString();

	}

	public static String metodoJSON(String metodo) {
		JSONObject obj = new JSONObject();
		obj.put("metodo", metodo);

		return obj.toJSONString();
	}

}
